package com.example.led;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorChangedListener;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView txtWarning;

    String IP = "http://192.168.1.8";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtWarning = findViewById(R.id.txtWarning);
        final SeekBar seekBarBrightness = findViewById(R.id.seekBarBrightness);
        final SeekBar seekBarVelocity = findViewById(R.id.seekBarVelocity);
        final TextView txtVelocity = findViewById(R.id.txtVelocity);
        final TextView txtProgress = findViewById(R.id.txtProgress);
        final ColorPickerView colorPickerView =  findViewById(R.id.color_picker_view);
        RadioGroup rg = findViewById(R.id.RGroup);

        String url = IP;
        getDataSet(url);
        Log.e("url: ",url);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = IP+"?p=0&r=0&g=0&b=0";
                getData(url);
                Log.e("url: ",url);
            }
        });

        FloatingActionButton fab2 = findViewById(R.id.fabOn);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = IP+"?p=100&r=100&g=100&b=100";
                getData(url);
                Log.e("url: ",url);
            }
        });

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        seekBarBrightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int selectedColor  = colorPickerView.getSelectedColor();
                int red = Color.red(selectedColor);
                red = red*100/255;
                int green = Color.green(selectedColor);
                green = green*100/255;
                int blue = Color.blue(selectedColor);
                blue = blue*100/255;
                int progress = seekBar.getProgress();
                txtProgress.setText("Brightness "+String.valueOf(progress)+"%");

                String url = IP+"?p="+String.valueOf(seekBarBrightness.getProgress())+"&r="+String.valueOf(red)+"&g="+String.valueOf(green)+"&b="+String.valueOf(blue);
                getData(url);
                Log.e("url: ",url);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
            {

            }
        });

        seekBarVelocity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //1 -> 1ms
                int progress = seekBar.getProgress();
                txtVelocity.setText("Velocity "+String.valueOf(progress)+" ms");

                String url = IP+"?velocity="+String.valueOf(progress);
                getData(url);
                Log.e("url: ",url);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser)
            {

            }
        });


        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                String url="";
                switch(checkedId)
                {
                    case R.id.rMode0:
                        url = IP+"?mode=0";
                        getData(url);
                        Log.e("url: ",url);
                        break;
                    case R.id.rMode1:
                        url = IP+"?mode=1";
                        getData(url);
                        Log.e("url: ",url);
                        break;
                    case R.id.rMode2:
                        url = IP+"?mode=2";
                        getData(url);
                        Log.e("url: ",url);
                        break;
                    case R.id.rMode3:
                        url = IP+"?mode=3";
                        getData(url);
                        Log.e("url: ",url);
                        break;
                    case R.id.rMode4:
                        url = IP+"?mode=4";
                        getData(url);
                        Log.e("url: ",url);
                        break;
                    case R.id.rMode5:
                        url = IP+"?mode=5";
                        getData(url);
                        Log.e("url: ",url);
                        break;
                    case R.id.rMode6:
                        url = IP+"?mode=6";
                        getData(url);
                        Log.e("url: ",url);
                        break;
                    case R.id.rMode7:
                        url = IP+"?mode=7";
                        getData(url);
                        Log.e("url: ",url);
                        break;
                }
            }
        });


        colorPickerView.addOnColorSelectedListener(new OnColorSelectedListener() {
            @Override
            public void onColorSelected(int selectedColor) {
                int red = Color.red(selectedColor);
                red = red*100/255;
                int green = Color.green(selectedColor);
                green = green*100/255;
                int blue = Color.blue(selectedColor);
                blue = blue*100/255;

                String url = IP+"?p="+String.valueOf(seekBarBrightness.getProgress())+"&r="+String.valueOf(red)+"&g="+String.valueOf(green)+"&b="+String.valueOf(blue);
                getData(url);
                Log.e("url: ",url);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void getData(String url)
    {
        if(isNetworkConnected())
        {
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(this);

            // Request a string response from the provided URL.
            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                Float Brightness = Float.parseFloat(response.getString("Brightness"));
                                int Red = Integer.parseInt(response.getString("Red"));
                                int Blue = Integer.parseInt(response.getString("Blue"));
                                int Green = Integer.parseInt(response.getString("Green"));
                                int modeLeds = Integer.parseInt(response.getString("modeLeds"));
                                int velocity = Integer.parseInt(response.getString("velocity"));

                                final SeekBar seekBarBrightness = findViewById(R.id.seekBarBrightness);
                                final SeekBar seekBarVelocity = findViewById(R.id.seekBarVelocity);
                                final TextView txtVelocity = findViewById(R.id.txtVelocity);
                                final TextView txtProgress = findViewById(R.id.txtProgress);
                                final ColorPickerView colorPickerView =  findViewById(R.id.color_picker_view);

                                RadioGroup rg = findViewById(R.id.RGroup);

                                Log.e("Error: ",String.valueOf(modeLeds));
                                switch (modeLeds){
                                    case 0:
                                        rg.check(R.id.rMode0);
                                        break;
                                    case 1:
                                        rg.check(R.id.rMode1);
                                        break;
                                    case 2:
                                        rg.check(R.id.rMode2);
                                        break;
                                    case 3:
                                        rg.check(R.id.rMode3);
                                        break;
                                    case 4:
                                        rg.check(R.id.rMode4);
                                        break;
                                    case 5:
                                        rg.check(R.id.rMode5);
                                        break;
                                    case 6:
                                        rg.check(R.id.rMode6);
                                        break;
                                    case 7:
                                        rg.check(R.id.rMode7);
                                        break;

                                }

                                txtWarning.setText("Brightness: "+String.valueOf(Brightness)
                                        +" Red: "+String.valueOf(Red)
                                        +" Blue: "+String.valueOf(Blue)
                                        +" Green: "+String.valueOf(Green)
                                        +" modeLeds: "+String.valueOf(modeLeds)
                                        +" velocity: "+String.valueOf(velocity)
                                );

                            } catch (JSONException e) {
                                e.printStackTrace();
                                txtWarning.setText("Response: " + e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            Log.e("Error: ",error.toString());

                        }
                    });

            jsObjRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            // Add the request to the RequestQueue.
            queue.add(jsObjRequest);

        }
        else
        {
            txtWarning.setText("Network is not connected!");
        }
    }

    private void getDataSet(String url)
    {
        if(isNetworkConnected())
        {
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(this);

            // Request a string response from the provided URL.
            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                Float Brightness = Float.parseFloat(response.getString("Brightness"));
                                int Red = Integer.parseInt(response.getString("Red"));
                                int Blue = Integer.parseInt(response.getString("Blue"));
                                int Green = Integer.parseInt(response.getString("Green"));
                                int modeLeds = Integer.parseInt(response.getString("modeLeds"));
                                int velocity = Integer.parseInt(response.getString("velocity"));

                                txtWarning.setText("Brightness: "+String.valueOf(Brightness)
                                        +" Red: "+String.valueOf(Red)
                                        +" Blue: "+String.valueOf(Blue)
                                        +" Green: "+String.valueOf(Green)
                                        +" modeLeds: "+String.valueOf(modeLeds)
                                        +" velocity: "+String.valueOf(velocity)
                                );


                            } catch (JSONException e) {
                                e.printStackTrace();
                                txtWarning.setText("Response: " + e.toString());
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            Log.e("Error: ",error.toString());

                        }
                    });

            jsObjRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            // Add the request to the RequestQueue.
            queue.add(jsObjRequest);

        }
        else
        {
            txtWarning.setText("Network is not connected!");
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
